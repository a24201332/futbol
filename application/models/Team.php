<?php
  /**
   *
   */
  class Team extends CI_Model
  {

    function __construct()
    {
      parent::__construct();
    }

    //nuevos hospitales
    function insertar($datos){
      $respuesta=$this->db->insert("equipo",$datos);
      return $respuesta;
    }


    //consulta de datosd
    function consultarTodos(){
      $teams=$this->db->get("equipo");
      if($teams->num_rows()>0){
        return $teams->result();
      } else {
        return false;
      }
    }


  function eliminar($id){
  $this->db->where("id_equi",$id);
  return $this->db->delete("equipo");
  }
  //Consulta de un solo hospital
      function obtenerPorId($id){
        $this->db->where("id_equi",$id);
        $teams=$this->db->get("equipo");
        if ($teams->num_rows()>0) {
          return $teams->row();
        } else {
          return false;
        }
      }
      //funcion para actualizar hospital{}
      //funcion para actualizar hospitales
        function actualizar($id,$datos){
          $this->db->where("id_equi",$id);
          return $this->db->update("equipo",$datos);
        }
    }

 ?>
