<?php
class Player extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    function insertar($datos)
    {
        $respuesta = $this->db->insert("jugador", $datos);
        return $respuesta;
    }

    function consultarTodos()
    {
        $this->db->select('jugador.*, posicion.nombre_pos, equipo.nombre_equi');
        $this->db->from('jugador');
        $this->db->join('posicion', 'jugador.fk_id_pos = posicion.id_pos', 'left');
        $this->db->join('equipo', 'jugador.fk_id_equi = equipo.id_equi', 'left');
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    function eliminar($id)
    {
        $this->db->where("id_jug", $id);
        return $this->db->delete("jugador");
    }

    function obtenerPorId($id)
    {
        $this->db->where("id_jug", $id);
        $query = $this->db->get("jugador");
        if ($query->num_rows() > 0) {
            return $query->row();
        } else {
            return false;
        }
    }

    function actualizar($id, $datos)
    {
        $this->db->where("id_jug", $id);
        return $this->db->update("jugador", $datos);
    }
}
?>
