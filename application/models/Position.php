<?php
  /**
   *
   */
  class Position extends CI_Model
  {

    function __construct()
    {
      parent::__construct();
    }

    //nuevos hospitales
    function insertar($datos){
      $respuesta=$this->db->insert("posicion",$datos);
      return $respuesta;
    }


    //consulta de datosd
    function consultarTodos(){
      $positions=$this->db->get("posicion");
      if($positions->num_rows()>0){
        return $positions->result();
      } else {
        return false;
      }
    }


  function eliminar($id){
  $this->db->where("id_pos",$id);
  return $this->db->delete("posicion");
  }
  //Consulta de un solo hospital
      function obtenerPorId($id){
        $this->db->where("id_pos",$id);
        $positions=$this->db->get("posicion");
        if ($positions->num_rows()>0) {
          return $positions->row();
        } else {
          return false;
        }
      }
      //funcion para actualizar hospital{}
      //funcion para actualizar hospitales
        function actualizar($id,$datos){
          $this->db->where("id_pos",$id);
          return $this->db->update("posicion",$datos);
        }
    }

 ?>
