<div class="container">
       <div class="row justify-content-center">
           <div class="col-md-8">
               <h1 class="text-center">
                   <i class="fa fa-plus-circle"></i>
                   Nuevo Equipo
               </h1>
               <br><br>
               <form method="post" action="<?php echo site_url('teams/guardarTeam'); ?>" enctype="multipart/form-data" id="formNuevoEquipo">
                   <div class="form-group row">
                       <label for="nombre_equi" class=""><b>Nombre:</b></label>
                           <input type="text" name="nombre_equi" id="nombre_equi" placeholder="Ingrese el nombre del equipo" class="form-control" required>
                   </div>

                   <div class="form-group row">
                       <label for="siglas_equi" class=""><b>Siglas:</b></label>
                           <input type="text" name="siglas_equi" id="siglas_equi" placeholder="Ingrese las siglas del equipo" class="form-control" required>
                   </div>

                   <div class="form-group row">
                       <label for="fundacion_equi" class=""><b>Año de fundación:</b></label>
                           <input type="number" name="fundacion_equi" id="fundacion_equi" placeholder="Ingrese el año de fundación del equipo" class="form-control" required>
                   </div>

                   <div class="form-group row">
                       <label for="region_equi" class=""><b>Región:</b></label>
                           <input type="text" name="region_equi" id="region_equi" placeholder="Ingrese la región del equipo" class="form-control" required>
                   </div>

                   <div class="form-group row">
                       <label for="numero_titulos_equi" class=""><b>Número de títulos:</b></label>
                           <input type="number" name="numero_titulos_equi" id="numero_titulos_equi" placeholder="Ingrese el número de titulos" class="form-control" required>
                   </div>

                   <div class="form-group row">
                       <div class="col-md-12 text-center">
                           <button type="submit" name="button" class="btn btn-primary">
                               <i class="fa fa-save"></i> Guardar
                           </button>
                           &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                           <a href="<?php echo site_url('positions/index'); ?>" class="btn btn-danger">
                               <i class="fa fa-times"></i> Cancelar
                           </a>
                       </div>
                   </div>
               </form>
           </div>
       </div>
   </div>
   <script>
       $(document).ready(function() {
           $('#formNuevoEquipo').validate({
               rules: {
                   nombre_equi: {
                       required: true
                   },
                   siglas_equi: {
                       required: true
                   },
                   fundacion_equi: {
                       required: true,
                       digits: true
                   },
                   region_equi: {
                       required: true
                   },
                   numero_titulos_equi: {
                       required: true,
                       digits: true
                   }
               },
               messages: {
                   nombre_equi: {
                       required: "Por favor ingrese el nombre del equipo"
                   },
                   siglas_equi: {
                       required: "Por favor ingrese las siglas del equipo"
                   },
                   fundacion_equi: {
                       required: "Por favor ingrese el año de fundación del equipo",
                       digits: "Ingrese solo dígitos"
                   },
                   region_equi: {
                       required: "Por favor ingrese la región del equipo"
                   },
                   numero_titulos_equi: {
                       required: "Por favor ingrese el número de títulos del equipo",
                       digits: "Ingrese solo dígitos"
                   }
               },
               errorElement: 'div',
               errorPlacement: function(error, element) {
                   error.addClass('invalid-feedback');
                   element.closest('.form-group').append(error);
               },
               highlight: function(element, errorClass, validClass) {
                   $(element).addClass('is-invalid');
               },
               unhighlight: function(element, errorClass, validClass) {
                   $(element).removeClass('is-invalid');
               }
           });
       });
   </script>
 </script>


<style media="screen">
.is-invalid {
border-color: #dc3545 !important;
}

.invalid-feedback {
color: #dc3545;
font-size: 0.875rem;
display: block;
margin-top: 0.25rem;
}

</style>
