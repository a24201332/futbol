
        <!-- ============================================================== -->
        <!-- End Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <div class="row page-titles">
                    <div class="col-md-6 col-8 align-self-center">
                        <h3 class="text-themecolor m-b-0 m-t-0">Teams</h3>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="<?php echo site_url(); ?>">Inicio</a></li>
                            <li class="breadcrumb-item active">Teams</li>
                        </ol>
                    </div>
                    <div class="col-md-6 col-4 align-self-center">
                        <a href="<?php echo site_url('teams/nuevo') ?>" class="btn pull-right hidden-sm-down btn-success">Agregar Equipo</a>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <div class="row">
                    <!-- column -->
                    <div class="col-sm-12">
                        <div class="card">
                            <div class="card-block">
                                <h4 class="card-title">Teams</h4>
                                <h6 class="card-subtitle">UTC -<code>Futbol</code></h6>
                                <div class="table-responsive">
                                    <?php if ($listadoTeams): ?>
                                    <table class="table" id="tbl_2">
                                        <thead>
                                            <tr>
                                                <th>ID</th>
                                                <th>Nombre</th>
                                                <th>Siglas</th>
                                                <th>Fundacion</th>
                                                <th>Region</th>
                                                <th>Numero de titulos</th>
                                                <th>Acciones</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                          <?php foreach ($listadoTeams as $teams): ?>
                                            <tr>
                                              <td><?php echo $teams->id_equi; ?></td>
                                              <td><?php echo $teams->nombre_equi; ?></td>
                                              <td><?php echo $teams->siglas_equi; ?></td>
                                              <td><?php echo $teams->fundacion_equi; ?></td>
                                              <td><?php echo $teams->region_equi; ?></td>
                                              <td><?php echo $teams->numero_titulos_equi; ?></td>
                                              <td>
                                                <a href="<?php echo site_url('teams/editar/').$teams->id_equi; ?>" class="btn btn-primary" title="Editar"><i class="fa fa-pen"></i></a>
                                                <a class="delete-team-btn btn btn-danger" href="#" data-id_equi="<?php echo $teams->id_equi; ?>"><i class="fa fa-trash"></i></a>
                                              </td>
                                            </tr>
                                          <?php endforeach; ?>
                                        </tbody>
                                    </table>
                                  <?php else: ?>
                                    <div class="alert alert-danger">
                                      No se ninguna posicion registrada
                                    </div>
                                  <?php endif; ?>
                                </div><br>

                            </div>
                        </div>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <script type="text/javascript">
                    $(document).ready(function() {
                        $('#tbl_2').DataTable( {
                            dom: 'Bfrtip',
                            buttons: [
                                {
                                    extend: 'pdfHtml5',
                                    text: '<i class="fa-solid fa-file-pdf"></i>',
                                    className: 'btn btn-info',
                                    messageTop: 'REPORTE DE EQUIPOS ',
                                    title:'INFORMACIÓN'
                                },
                                {
                                    extend: 'print',
                                    text: '<i class="fa-solid fa-print"></i>',
                                    className: 'btn btn-info',
                                    messageTop: 'REPORTE DE EQUIPOS',
                                    title:'INFORMACIÓN'
                                },
                                {
                                    extend: 'csv',
                                    text: '<i class="fa-solid fa-file-csv"></i>',
                                    className: 'btn btn-info',
                                    messageTop: 'REPORTE DE EQUIPOS ',
                                    title:'INFORMACIÓN'
                                }
                            ],
                            language: {
                                url: "https://cdn.datatables.net/plug-ins/1.10.25/i18n/Spanish.json"
                            }
                        } );
                    } );
                </script>

                <script>
            $(document).ready(function() {
                $('.delete-team-btn').click(function(event) {
                    // Evitar el comportamiento predeterminado del enlace
                    event.preventDefault();

                    // Obtener el ID del atributo data-id_equi
                    var id = $(this).data('id_equi');
                    Swal.fire({
                        title: '¿Estás seguro de que quieres eliminar este registro?',
                        showDenyButton: true,
                        showCancelButton: true,
                        confirmButtonText: 'Sí',
                        denyButtonText: 'No',
                        customClass: {
                            actions: 'my-actions',
                            cancelButton: 'order-1 right-gap',
                            confirmButton: 'order-2',
                            denyButton: 'order-3',
                        },
                    }).then((result) => {
                        if (result.isConfirmed) {
                            // Realizar la acción de eliminación
                            $.ajax({
                                type: 'GET',
                                url: "<?php echo site_url('teams/borrar/'); ?>" + id,
                                success: function(response) {
                                    // Si la eliminación es exitosa, puedes mostrar un mensaje o actualizar la tabla
                                    Swal.fire('¡Eliminado!', 'El registro ha sido eliminado.', 'success').then(() => {
                                        // Puedes redirigir a otra página o actualizar la tabla aquí si es necesario
                                        window.location.reload(); // Ejemplo de recargar la página
                                    });
                                },
                                error: function() {
                                    // Manejar errores si la eliminación no tiene éxito
                                    Swal.fire('Error', 'Hubo un problema al eliminar el registro.', 'error');
                                }
                            });
                        } else if (result.isDenied) {
                            // No hacer nada si el usuario cancela la eliminación
                        }
                    });
                });
            });
            </script>
