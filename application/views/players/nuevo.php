<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <h1 class="text-center">
                <i class="fa fa-plus-circle"></i>
                Nuevo Jugador
            </h1>
            <br><br>
            <form method="post" action="<?php echo site_url('players/guardarPlayer'); ?>" enctype="multipart/form-data" id="formNuevoJugador">
                <div class="form-group row">
                    <label for="apellido_jug" class=""><b>Apellido:</b></label>
                        <input type="text" name="apellido_jug" id="apellido_jug" placeholder="Ingrese el apellido del jugador" class="form-control" required>
                </div>

                <div class="form-group row">
                    <label for="nombre_jug" class=""><b>Nombre:</b></label>
                         <input type="text" name="nombre_jug" id="nombre_jug" placeholder="Ingrese el nombre del jugador" class="form-control" required>                </div>

                <div class="form-group row">
                    <label for="estatura_jug" class=""><b>Estatura:</b></label>
                        <input type="number" name="estatura_jug" id="estatura_jug" placeholder="Ingrese la estatura del jugador" class="form-control" required>
                </div>

                <div class="form-group row">
                    <label for="salario_jug" class=""><b>Salario:</b></label>
                        <input type="text" name="salario_jug" id="salario_jug" placeholder="Ingrese el salario del jugador" class="form-control" required>
                </div>

                <div class="form-group row">
                  <label for="estado_jug" class=""><b>Estado:</b></label>
                      <select name="estado_jug" id="estado_jug" class="form-control" required>
                          <option value="">Seleccione el estado</option>
                          <option value="Activo">Activo</option>
                          <option value="Inactivo">Inactivo</option>
                      </select>
              </div>


                <div class="form-group row">
                    <label for="fk_id_pos" class=""><b>Posición:</b></label>
                      <select class="form-control" id="fk_id_pos" name="fk_id_pos" required>
                          <option value="">Seleccione una posición</option>
                          <?php foreach ($posiciones as $posicion): ?>
                              <option value="<?php echo $posicion->id_pos; ?>"><?php echo $posicion->nombre_pos; ?></option>
                          <?php endforeach; ?>
                      </select>

                </div>

                <div class="form-group row">
                    <label for="fk_id_equi" class=""><b>Equipo:</b></label>
                        <select name="fk_id_equi" id="fk_id_equi" class="form-control" required>
                            <option value="">Seleccione un equipo</option>
                            <?php foreach ($equipos as $equipo): ?>
                                <option value="<?php echo $equipo->id_equi; ?>"><?php echo $equipo->nombre_equi; ?></option>
                            <?php endforeach; ?>
                        </select>
                </div>

                <div class="form-group row">
                    <div class="col-md-12 text-center">
                        <button type="submit" name="button" class="btn btn-primary">
                            <i class="fa fa-save"></i> Guardar
                        </button>
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <a href="<?php echo site_url('players/index'); ?>" class="btn btn-danger">
                            <i class="fa fa-times"></i> Cancelar
                        </a>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>




<script>
$(document).ready(function() {
    $.validator.addMethod("validChars", function(value, element) {
        return this.optional(element) || /^[\d.,]+$/.test(value);
    }, "Solo se permiten números, puntos y comas.");

    $.validator.addMethod("maxHeight", function(value, element) {
        return this.optional(element) || parseFloat(value.replace(',', '.')) <= 2.50;
    }, "La estatura no puede ser mayor a 2.50 metros.");

    $("#formNuevoJugador").validate({
        rules: {
            apellido_jug: {
                required: true,
                minlength: 3,
                maxlength: 50,
                lettersonly: true
            },
            nombre_jug: {
                required: true,
                maxlength: 50,
                lettersonly: true
            },
            estatura_jug: {
                required: true,
                validChars: true,
                maxHeight: true
            },
            salario_jug: {
                required: true,
                validChars: true
            },
            estado_jug: {
                required: true
            },
            fk_id_pos: {
                required: true
            },
            fk_id_equi: {
                required: true
            }
        },
        messages: {
            apellido_jug: {
                required: "Por favor ingrese el apellido del jugador.",
                minlength: "El apellido debe tener al menos 3 caracteres.",
                maxlength: "El apellido no puede tener más de 50 caracteres.",
                lettersonly: "Solo se permiten letras en el apellido."
            },
            nombre_jug: {
                required: "Por favor ingrese el nombre del jugador.",
                maxlength: "El nombre no puede tener más de 50 caracteres.",
                lettersonly: "Solo se permiten letras en el nombre."
            },
            estatura_jug: {
                required: "Por favor ingrese la estatura del jugador.",
                validChars: "Solo se permiten números, puntos y comas en la estatura.",
                maxHeight: "La estatura no puede ser mayor a 2.50 metros."
            },
            salario_jug: {
                required: "Por favor ingrese el salario del jugador.",
                validChars: "Solo se permiten números, puntos y comas en el salario."
            },
            estado_jug: {
                required: "Por favor seleccione el estado del jugador."
            },
            fk_id_pos: {
                required: "Por favor seleccione una posición."
            },
            fk_id_equi: {
                required: "Por favor seleccione un equipo."
            }
        },
        errorClass: "is-invalid",
        errorElement: "div",
        errorPlacement: function(error, element) {
            error.addClass("invalid-feedback");
            element.closest(".form-group").append(error);
        },
        highlight: function(element, errorClass, validClass) {
            $(element).addClass(errorClass).removeClass(validClass);
        },
        unhighlight: function(element, errorClass, validClass) {
            $(element).removeClass(errorClass).addClass(validClass);
        }
    });
});
</script>


<style media="screen">
.is-invalid {
  border-color: #dc3545 !important;
}

.invalid-feedback {
  color: #dc3545;
  font-size: 0.875rem;
  display: block;
  margin-top: 0.25rem;
}
</style>
