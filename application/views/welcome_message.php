<!-- ============================================================== -->
			<!-- End Left Sidebar - style you can find in sidebar.scss  -->
			<!-- ============================================================== -->
			<!-- ============================================================== -->
			<!-- Page wrapper  -->
			<!-- ============================================================== -->
			<div class="page-wrapper">
					<!-- ============================================================== -->
					<!-- Container fluid  -->
					<!-- ============================================================== -->
					<div class="container-fluid">
							<!-- ============================================================== -->
							<!-- Bread crumb and right sidebar toggle -->
							<!-- ============================================================== -->
							<div class="row page-titles">
									<div class="col-md-6 col-8 align-self-center">
											<h3 class="text-themecolor m-b-0 m-t-0">Dashboard</h3>
											<ol class="breadcrumb">
													<li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
													<li class="breadcrumb-item active">Dashboard</li>
											</ol>
									</div>
									<div class="col-md-6 col-4 align-self-center">
											<a href="https://themewagon.com/themes/free-bootstrap-4-admin-dashboard-template/" class="btn pull-right hidden-sm-down btn-success">Download Here</a>
									</div>
							</div>
							<!-- ============================================================== -->
							<!-- End Bread crumb and right sidebar toggle -->
							<!-- ============================================================== -->
							<!-- ============================================================== -->
							<!-- Start Page Content -->
							<!-- ============================================================== -->
							<!-- Row -->
							<div class="row">
									<!-- Column -->
									<div class="col-sm-6">
											<div class="card">
													<div class="card-block">
															<h4 class="card-title">Costa Team </h4>
															<div class="text-right">
																	<h2 class="font-light m-b-0"><i class="ti-arrow-up text-success"></i> $50,000</h2>
																	<span class="text-muted">Salary</span>
															</div>
															<span class="text-success">80%</span>
															<div class="progress">
																	<div class="progress-bar bg-danger" role="progressbar" style="width: 80%; height: 6px;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
															</div>
													</div>
											</div>
									</div>
									<!-- Column -->
									<!-- Column -->
									<div class="col-sm-6">
											<div class="card">
													<div class="card-block">
															<h4 class="card-title">Sierra Team</h4>
															<div class="text-right">
																	<h2 class="font-light m-b-0"><i class="ti-arrow-up text-info"></i> $20,000</h2>
																	<span class="text-muted">Salary</span>
															</div>
															<span class="text-info">30%</span>
															<div class="progress">
																	<div class="progress-bar bg-info" role="progressbar" style="width: 30%; height: 6px;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
															</div>
													</div>
											</div>
									</div>
									<!-- Column -->
							</div>
							<!-- Row -->
							<!-- Row -->
							<div class="col-sm-12">
							        <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
							            <ol class="carousel-indicators">
							                <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
							                <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
							                <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
							                <!-- Add more indicators as needed -->
							            </ol>
							            <div class="carousel-inner">
							                <div class="carousel-item active">
							                    <img class="d-block w-100" src="https://studiofutbol.com.ec/wp-content/uploads/2024/02/GHJXGMLW8AAT1Km.jpeg" alt="First slide">
							                </div>
							                <div class="carousel-item">
							                    <img class="d-block w-100" src="https://imagenes.extra.ec/files/image_full/uploads/2022/10/01/6338d02ccb9b0.jpeg" alt="Second slide">
							                </div>

															<div class="carousel-item">
																 <img class="d-block w-100" src="https://i.ytimg.com/vi/urjS9C4RLUs/maxresdefault.jpg" alt="Third slide">
														 </div>
							                <!-- Add more carousel items as needed -->
							            </div>
							            <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
							                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
							                <span class="sr-only">Previous</span>
							            </a>
							            <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
							                <span class="carousel-control-next-icon" aria-hidden="true"></span>
							                <span class="sr-only">Next</span>
							            </a>
							        </div>
							    </div>
							    <!-- column -->
							</div>
							<!-- Row -->
							<!-- Row -->
							<div class="row">
									<div class="col-sm-12">
											<div class="card">
													<div class="card-block">

															<h4 class="card-title">JUGADORES DEL BSC</h4>
															<div class="table-responsive m-t-40">
																	<table class="table stylish-table">
																			<thead>
																					<tr>
																							<th colspan="2">Asignado</th>
																							<th>Nombre</th>
																							<th>Salario</th>
																					</tr>
																			</thead>
																			<tbody>
																					<tr>
																						<td><span class="round"><img src="https://www.directvsports.com/__export/1703607771751/sites/dsports/img/2023/12/26/20231226_012251177_ry3ic9qrl7uc15chdoffsczht.jpg_1301049368.jpg" alt="user" width="50" /></span></td>
																							<td>
																									<h6>Iñaqui</h6><small class="text-muted">Portero</small></td>
																							<td>Iñaqui Peña</td>
																							<td>$40.000</td>
																					</tr>
																					<tr class="active">
																							<td><span class="round"><img src="https://assets.laliga.com/squad/2024/t178/p493019/2048x2048/p493019_t178_2024_0_002_000.jpg" alt="user" width="50" /></span></td>
																							<td>
																									<h6>Andrew</h6><small class="text-muted">Defensa Izquierda</small></td>
																							<td>Alejandro Balde</td>
																							<td>$50.000</td>
																					</tr>
																					<tr>

																							<td><span class="round round-success"><img src="https://assets.laliga.com/squad/2024/t178/p90318/2048x2048/p90318_t178_2024_0_002_000.jpg" alt="user" width="50" /></span></td>
																							<td>
																									<h6>Mart</h6><small class="text-muted">Defensa Derecha</small></td>
																							<td>Iñigo Martinez</td>
																							<td>$60.000</td>
																					</tr>
																					<tr>
																							<td><span class="round round-primary"><img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRFB0JZS7Vm6qVP5dsafDjPuKHoN_OkzI8bSYSk3dhl5wk5CRTtvcfAz3ODZWPG3M4Jdos&usqp=CAU" alt="user" width="50" /></span></td>
																							<td>
																									<h6>Gavi</h6><small class="text-muted">Centro Campista</small></td>
																							<td>Gavi Monetenegro</td>
																							<td>$50.000</td>
																					</tr>
																					<tr>
																							<td><span class="round round-warning"><img src="https://i.pinimg.com/originals/71/c3/d3/71c3d305e7505355b013c61261933838.png" alt="user" width="50" /></span></td>
																							<td>
																									<h6>Ferran Torres</h6><small class="text-muted">Delanteros</small></td>
																							<td>Vitor Roque</td>
																							<td>$40.000</td>
																					</tr>
																					<tr>
																							<td><span class="round round-danger"><img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSYMFFwxsterNDAl94y0AFfo0j8xoXS0LG_QQ&s" alt="user" width="50" /></span></td>
																							<td>
																									<h6>Johna</h6><small class="text-muted">Delanteros</small></td>
																							<td>Johnathan Duque</td>
																							<td>$60.000</td>
																					</tr>
																			</tbody>
																	</table>
															</div>
													</div>
											</div>
									</div>
							</div>
							<!-- Row -->
							<!-- Row -->
							<div class="row">
									<!-- Column -->
									<div class="col-lg-4">
											<div class="card">
													<img class="card-img-top img-responsive" src="https://i.ytimg.com/vi/5Xu7vzqLFKY/hq720.jpg?sqp=-oaymwEhCK4FEIIDSFryq4qpAxMIARUAAAAAGAElAADIQj0AgKJD&rs=AOn4CLAOB-x9DuDR8WriNB6oA16xO1m_dg" alt="Card">
													<div class="card-block">
															<ul class="list-inline font-14">
																	<li class="p-l-0">27 Jun 2024</li>
																	<li><a href="javascript:void(0)" class="link">3 Comment</a></li>
															</ul>
															<h3 class="font-normal">Mexico Vs Ecuador
																<br>
																 3-1</h3>
													</div>
											</div>
									</div>
									<!-- Column -->
									<!-- Column -->
									<div class="col-lg-4">
											<div class="card">
													<img class="card-img-top img-responsive" src="https://img.youtube.com/vi/knkxu0mp9NQ/maxresdefault.jpg" alt="Card">
													<div class="card-block">
															<ul class="list-inline font-14">
																	<li class="p-l-0">27 Jun 2024</li>
																	<li><a href="javascript:void(0)" class="link">5 Comment</a></li>
															</ul>
															<h3 class="font-normal">Venezuela Vs Mexico
																<br>
																1-0
															</h3>
													</div>
											</div>
									</div>
									<!-- Column -->
									<!-- Column -->
									<div class="col-lg-4">
											<div class="card">
													<img class="card-img-top img-responsive" src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQIUK9Fg-b1O1RziGrAA0U4TUzUQd6mv9gbLqgTcSCG60yWYQr_ARK0ec26mU4xY6U4ubg&usqp=CAU" alt="Card">
													<div class="card-block">
															<ul class="list-inline font-14">
																	<li class="p-l-0">20 May 2024</li>
																	<li><a href="javascript:void(0)" class="link">6 Comment</a></li>
															</ul>
															<h3 class="font-normal">Venezuela Vs Chile
																<br>
																3-0
															</h3>
													</div>
											</div>
									</div>
									<!-- Column -->
							</div>
							<!-- Row -->
							<!-- ============================================================== -->
							<!-- End PAge Content -->
							<!-- ============================================================== -->
					</div>
					<!-- ============================================================== -->
					<!-- End Container fluid  -->
					<!-- ============================================================== -->
					<!-- ============================================================== -->
