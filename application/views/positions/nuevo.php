<div class="container">
       <div class="row justify-content-center">
           <div class="col-md-8">
               <h1 class="text-center">
                   <i class="fa fa-plus-circle"></i>
                   Nueva Posición del Jugador
               </h1>
               <br><br>
               <form method="post" action="<?php echo site_url('positions/guardarPosition'); ?>" enctype="multipart/form-data" id="formPosition">

                   <div class="form-group row">
                       <label for="nombre_pos" class=""><b>Nombre:</b></label>
                       <input type="text" name="nombre_pos" id="nombre_pos" placeholder="Ingrese el nombre de la posición" class="form-control" required>
                   </div>

                   <div class="form-group row">
                       <label for="descripcion_pos" class=""><b>Descripción:</b></label>
                       <input type="text" name="descripcion_pos" id="descripcion_pos" placeholder="Ingrese la descripción de la posición" class="form-control" required>
                       <div class="col-md-9">
                       </div>
                   </div>

                   <div class="form-group row">
                       <div class="col-md-12 text-center">
                           <button type="submit" name="button" class="btn btn-primary">
                               <i class="fa fa-save"></i> Guardar
                           </button>
                           &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                           <a href="<?php echo site_url('positions/index'); ?>" class="btn btn-danger">
                               <i class="fa fa-times"></i> Cancelar
                           </a>
                       </div>
                   </div>
               </form>
           </div>
       </div>
   </div>


   <script>
       $(document).ready(function() {
           // Definir regla personalizada para permitir letras, punto (.), y otros caracteres especiales
           $.validator.addMethod("lettersonlywithspecial", function(value, element) {
               return this.optional(element) || /^[a-zA-Z .,]+$/.test(value);
           }, "Ingrese solo letras, puntos, y caracteres especiales y (.,)");

           // Configurar validación para el formulario
           $('#formPosition').validate({
               rules: {
                   nombre_pos: {
                       required: true,
                       minlength: 3,
                       maxlength: 50,
                       lettersonlywithspecial: true
                   },
                   descripcion_pos: {
                       required: true,
                       minlength: 5,
                       maxlength: 200,
                       lettersonlywithspecial: true
                   }
               },
               messages: {
                   nombre_pos: {
                       required: "Por favor ingrese el nombre de la posición",
                       minlength: "El nombre debe tener al menos {0} caracteres",
                       maxlength: "El nombre no debe exceder los {0} caracteres",
                       lettersonlywithspecial: "Ingrese solo letras no números y (.,)"
                   },
                   descripcion_pos: {
                       required: "Por favor ingrese la descripción de la posición",
                       minlength: "La descripción debe tener al menos {0} caracteres",
                       maxlength: "La descripción no debe exceder los {0} caracteres",
                       lettersonlywithspecial: "Ingrese solo letras no números y (.,)"
                   }
               },
               errorElement: 'div',
               errorPlacement: function(error, element) {
                   error.addClass('invalid-feedback');
                   element.closest('.form-group').append(error);
               },
               highlight: function(element, errorClass, validClass) {
                   $(element).addClass('is-invalid');
               },
               unhighlight: function(element, errorClass, validClass) {
                   $(element).removeClass('is-invalid');
               }
           });
       });
   </script>


<style media="screen">
.is-invalid {
  border-color: #dc3545 !important;
}

.invalid-feedback {
  color: #dc3545;
  font-size: 0.875rem;
  display: block;
  margin-top: 0.25rem; 
}

</style>
