<?php
    error_reporting(0);
  /**
   *
   */
  class Positions extends CI_Controller
  {

    function __construct(){
      parent::__construct();
      $this->load->model("Position");


    }

    public function index(){
      $data["listadoPositions"]=$this->Position->consultarTodos();
      $this->load->view('header');
      $this->load->view('positions/index',$data);
      $this->load->view('footer');
    }

    public function borrar($id_pos){
      $this->Position->eliminar($id_pos);
      $this->session->set_flashdata("confirmacion","Posicion eliminado exitosamente");
      redirect("positions/index");
    }

    //Renderización formulario dehhrtgyut nuevo Hospitales
    public function nuevo(){
      $this->load->view("header");
      $this->load->view("positions/nuevo");
      $this->load->view("footer");
    }

    //capturando datos e insertanderrtgtfo en la taddbla
    public function guardarPosition(){
      $datosNuevoPosition=array(
        "nombre_pos"=>$this->input->post("nombre_pos"),
        "descripcion_pos"=>$this->input->post("descripcion_pos")


      );
      $this->Position->insertar($datosNuevoPosition);
      $this->session->set_flashdata("confirmacion","Positions guardado exitosamente");
      redirect('positions/index');
    }
    //editar
    public function editar($id){
      $data["positionEditar"]=$this->Position->obtenerPorId($id);
      $this->load->view('header');
      $this->load->view('positions/editar',$data);
      $this->load->view('footer');
    }

    public function actualizarPosition(){
        $id_pos = $this->input->post("id_pos");
        $datosActualizar = array(
            "nombre_pos" => $this->input->post("nombre_pos"),
            "descripcion_pos" => $this->input->post("descripcion_pos")
        );

        $this->Position->actualizar($id_pos, $datosActualizar);
        $this->session->set_flashdata("confirmacion", "Posición actualizada exitosamente");
        redirect('positions/index');
    }




  }// fin de clase


 ?>
