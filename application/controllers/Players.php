<?php
class Players extends CI_Controller
{
    function __construct(){
        parent::__construct();
        $this->load->model("Player");
        $this->load->model("Position");
        $this->load->model("Team");
    }

    public function index(){
        $data["listadoPlayers"] = $this->Player->consultarTodos();
        $this->load->view('header');
        $this->load->view('players/index', $data);
        $this->load->view('footer');
    }

    public function borrar($id_jug){
        $this->Player->eliminar($id_jug);
        $this->session->set_flashdata("confirmacion", "Jugador eliminado exitosamente");
        redirect("players/index");
    }

    public function nuevo(){
        $data["posiciones"] = $this->Position->consultarTodos();
        $data["equipos"] = $this->Team->consultarTodos();
        $this->load->view("header");
        $this->load->view("players/nuevo", $data);
        $this->load->view("footer");
    }

    public function guardarPlayer(){
        $datosNuevoPlayer = array(
            "apellido_jug" => $this->input->post("apellido_jug"),
            "nombre_jug" => $this->input->post("nombre_jug"),
            "estatura_jug" => $this->input->post("estatura_jug"),
            "salario_jug" => $this->input->post("salario_jug"),
            "estado_jug" => $this->input->post("estado_jug"),
            "fk_id_pos" => $this->input->post("fk_id_pos"),
            "fk_id_equi" => $this->input->post("fk_id_equi")
        );
        $this->Player->insertar($datosNuevoPlayer);
        $this->session->set_flashdata("confirmacion", "Jugador guardado exitosamente");
        redirect('players/index');
    }

    public function editar($id){
          $data["playerEditar"] = $this->Player->obtenerPorId($id);
          $data["posiciones"] = $this->Position->consultarTodos();
          $data["equipos"] = $this->Team->consultarTodos();
          $this->load->view('header');
          $this->load->view('players/editar', $data);
          $this->load->view('footer');
      }

    public function actualizarPlayer(){
        $id_jug = $this->input->post("id_jug");
        $datosActualizar = array(
            "apellido_jug" => $this->input->post("apellido_jug"),
            "nombre_jug" => $this->input->post("nombre_jug"),
            "estatura_jug" => $this->input->post("estatura_jug"),
            "salario_jug" => $this->input->post("salario_jug"),
            "estado_jug" => $this->input->post("estado_jug"),
            "fk_id_pos" => $this->input->post("fk_id_pos"),
            "fk_id_equi" => $this->input->post("fk_id_equi")
        );
        $this->Player->actualizar($id_jug, $datosActualizar);
        $this->session->set_flashdata("confirmacion", "Jugador actualizado exitosamente");
        redirect('players/index');
    }
}
?>
