<?php
    error_reporting(0);
  /**
   *
   */
  class Teams extends CI_Controller
  {

    function __construct(){
      parent::__construct();
      $this->load->model("Team");


    }

    public function index(){
      $data["listadoTeams"]=$this->Team->consultarTodos();
      $this->load->view('header');
      $this->load->view('teams/index',$data);
      $this->load->view('footer');
    }

    public function borrar($id_equi){
      $this->Team->eliminar($id_equi);
      $this->session->set_flashdata("confirmacion","Equipo eliminado exitosamente");
      redirect("teams/index");
    }

    //Renderización formulario dehhrtgyut nuevo Hospitales
    public function nuevo(){
      $this->load->view("header");
      $this->load->view("teams/nuevo");
      $this->load->view("footer");
    }

    //capturando datos e insertanderrtgtfo en la taddbla
    public function guardarTeam(){
      $datosNuevoTeam=array(
        "nombre_equi"=>$this->input->post("nombre_equi"),
        "siglas_equi"=>$this->input->post("siglas_equi"),
        "fundacion_equi"=>$this->input->post("fundacion_equi"),
        "region_equi"=>$this->input->post("region_equi"),
        "numero_titulos_equi"=>$this->input->post("numero_titulos_equi")
      );

      $this->Team->insertar($datosNuevoTeam);
      $this->session->set_flashdata("confirmacion","Equipos guardado exitosamente");
      redirect('teams/index');
    }
    //editar
    public function editar($id){
      $data["teamEditar"]=$this->Team->obtenerPorId($id);
      $this->load->view('header');
      $this->load->view('teams/editar',$data);
      $this->load->view('footer');
    }

    public function actualizarTeam(){
        $id_equi = $this->input->post("id_equi");
        $datosActualizar = array(
            "nombre_equi" => $this->input->post("nombre_equi"),
            "siglas_equi" => $this->input->post("siglas_equi"),
            "fundacion_equi" => $this->input->post("fundacion_equi"),
            "region_equi" => $this->input->post("region_equi"),
            "numero_titulos_equi" => $this->input->post("numero_titulos_equi")


        );

        $this->Team->actualizar($id_equi, $datosActualizar);
        $this->session->set_flashdata("confirmacion", "Equipo actualizada exitosamente");
        redirect('teams/index');
    }




  }// fin de clase


 ?>
